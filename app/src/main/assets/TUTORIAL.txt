thLight
Bem vindo ao AEditor!

Dicas básicas:
Para criar um arquivo, toque o primeiro botão à esquerda na barra de ação;
Para salvar o texto, toque o botão do meio no editor;
Para abrir outro arquivo, toque o botão à direita da barra de ação no editor para voltar e, na tela inicial, o botão do meio;
Na tela inicial, o ícone do AEditor retorna ao editor sem comandos adicionais. Caso estivera criando um texto novo e saiu do editor, tocar  o botão AE retornará ao editor imediatamente com o estado de edição anterior*

Veja informações sobre o AEditor tocando na barra de status.

Dicas avançadas:
Customizar tema e estilo, cor e tamanho das fontes dos com extensão ".adit" é um recurso adicional do AEditor.

Adicione os códigos seguintes no início do texto, separados com uma quebra de linha e salve o arquivo para ver as alterações.
Tema:
   thLight: tema claro
   thRed: tema vermelho
   thBlack: tema escuro
   thDefault: tema amarelo (padrão)
Fonte:
   tf<Normal/Mono/Serif>: altera o estilo da fonte
   tc<COR>: muda a cor do texto
   tz<TAMANHO>: muda o tamanho da fonte

Você também pode alterar isso pela caixa de ferramentas de texto. Toque no nome do arquivo para abrir a caixa de ferramentas.

*: isso só funciona se "Manter alterações no texto após sair" estiver ativo.

by cleds'upper
