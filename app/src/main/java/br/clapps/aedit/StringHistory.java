/*
 * Copyright © 2015-2018 Cledson Ferreira <cledsonitgames@gmail.com>
 * This work is free. You can redistribute it and/or modify it under the
 * terms of the Do What The Fuck You Want To Public License, Version 2,
 * as published by Sam Hocevar. See the COPYING file for more details.
*/

package br.clapps.aedit;

/*
 * by cleds'upper! (Cledson Ferreira)
 */

public class StringHistory {
    private String[] history;
    private int caret = 0; // o caret, por padrão, sempre aponta para a última String
    private int limit; // todo histórico tem um limite!

    /**
     * Inicializa o histórico de Strings (ou textos)
     *
     * @param str: Primeira String que será salva
     * @param limit: Número máximo de strings
     */
    StringHistory(String str, int limit) {
        if (str != null) {
            history = new String[1];
            history[0] = str;
        }
        this.limit = limit;
    }

    /**
     * Adiciona uma String ao fim do StringHistory
     *
     * @param str: A String a ser adicionada no final (sempre) do histórico
     * @return: verdadeiro se salvo
     */
    public boolean append(String str) {
        if (str == null) {
            return false;
        }

        String new_history[];
        int length;
        if (history != null) {
            if (history.length >= limit - 1) {
                if (!remove(1)) {
                    remove(0);
                }
            }
            if (history == null) {
                new_history = new String[1];
                length = 0;
            } else {
                new_history = new String[history.length + 1];
                for (int i = 0; i < history.length; i++) {
                    new_history[i] = history[i];
                }
                length = history.length;
            }
        } else {
            new_history = new String[1];
            length = 0;
        }

        caret = length;
        new_history[length] = str;
        history = new_history;
        return true;
    }

    public void clear() {
        history = null;
        caret = 0;
    }

    public void clearFromCaret() {
		if (history == null) return;
        if (caret < history.length - 1) {
            int caret = this.caret;
            for (int c = history.length - caret; c > 0; c--) {
                remove(caret);
            }
            moveCaret(getEnd());
        }
    }

    /**
     * Caso encontre uma String igual no histórico, retorna a posição dela
     *
     * @param str:
     */
    public int get(String str) {
        int s = 0;
        if (str == null || history == null) {
            return -1;
        }
        for (; s < history.length; s++) {
            if (history[s] != null) {
                if (history[s].equals(str)) {
                    break;
                } else if (history[history.length - 1 - s].equals(str)) {
                    s = history.length - 1 - s;
                    break;
                }
            }
        }
        return s;
    }

    public int getCaret() {
        return caret;
    }

    public int getEnd() {
        if (history == null) {
            return -1;
        }
        int end = history.length - 1;
        return end;
    }

    public String getEndString() {
        if (history != null) {
            return history[getEnd()];
        }
        return "";
    }

    public int getStart() {
		if (history==null) return -1;
        return 0;
    }

    public String getStartString() {
        if (history != null) {
            return history[0];
        }
        return "";
    }

    public String getString() {
		if (history == null) return null;
		if (caret<0 || caret>=history.length) caret = 0;
        return history[caret];
    }

    public String getString(int p) {
        return history[p];
    }

	public boolean has() {
		return history!=null;
	}
    /**
     * Verifica se uma String semelhante existe
     *
     * @return: Verdadeiro somente se a String existir
     */
    public boolean has(String str) {
        if (str == null || history == null) {
            return false;
        }
        for (int s = 0; s < history.length; s++) {
            if (history[s] != null) {
                if (history[s].equals(str)) {
                    return true;
                } else if (history[history.length - 1 - s].equals(str)) {
                    return true;
                }
            }
        }
        return false;
    }

    public void moveCaret(int caret) {
		if (history == null) {
			this.caret = 0;
		}
        else if (caret >= 0 && caret < history.length) {
            this.caret = caret;
        }
    }

    public void moveCaretToLeft() {
        if (caret > 0) {
            caret--;
        }
    }

    public void moveCaretToRight() {
        if (history != null) {
            if (caret < history.length - 1) {
                caret++;
            }
        } else {
			caret = 0;
		}
    }

    /**
     * Remove uma string do histórico
     *
     * @param s: posição onde se encontra a string
     * @return: verdadeiro se a remoção for concluída
     */
    public boolean remove(int s) {
        if (history == null) {
            return false;
        } else if (s >= history.length || s < 0) {
			return false;
		}

		String[] new_history = new String[history.length-1];
        for (int i = 0; i < history.length; i++) {
            if (i == s && i > 0) {
                for (int j = i; j < history.length - 1; j++) {
                    new_history[j] = history[j + 1];
                }
                break;
            } else if (i == s) {
                history = null;
                caret = 0;
                return true;
            } else{
				new_history[i] = history[i];
			}
        }

        if (caret >= new_history.length && new_history.length > 0) {
            caret = new_history.length - 1;
        } else {
            caret = 0;
        }
		history = new_history;
        return true;
    }

    public void setString(String str, int c){
        if(history!=null){
            if(c<history.length){
                history[c] = str;
            }
        }
    }
}
