/*
 * Copyright © 2015-2018 Cledson Ferreira <cledsonitgames@gmail.com>
 * This work is free. You can redistribute it and/or modify it under the
 * terms of the Do What The Fuck You Want To Public License, Version 2,
 * as published by Sam Hocevar. See the COPYING file for more details.
*/

package br.clapps.aedit;
import android.os.Build;
import android.widget.*;
import android.content.*;
import android.view.*;
import android.graphics.*;
import android.graphics.drawable.*;

/*
 * by cleds'upper (Cledson Ferreira)
 */

public class NoteView{
	private LinearLayout linearLayout;
	private RelativeLayout bar;
	private ImageView barClose;
	private TextView barTitle;
	private TextView text;

	public NoteView(Context context){
		linearLayout = new LinearLayout(context);
		linearLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, (int)(280*context.getResources().getDisplayMetrics().density)));
		linearLayout.setOrientation(LinearLayout.VERTICAL);
		linearLayout.setPadding(11, 11, 11, 11);

		barClose = new ImageView(context);
		barTitle = new TextView(context);
		bar = new RelativeLayout(context);
		text = new TextView(context);

		bar.setBackgroundColor(0xfff1b100);

		barTitle.setPadding(11, 11, 11, 11);
		barTitle.setSingleLine(true);
		barTitle.setGravity(Gravity.CENTER_VERTICAL);
		barTitle.setTextSize(18);
		barTitle.setTextColor(Color.WHITE);
		barTitle.setText(R.string.title_newbuffer);

		barClose.setContentDescription(context.getString(R.string.removeNoteAccessibility));
		barClose.setImageDrawable(context.getResources().getDrawable(R.drawable.bt_close));

		text.setBackgroundColor(0xfff0f0f0);
		text.setPadding(16, 16, 16, 16);
		text.setTextColor(0xff008b8b);
		text.setTypeface(Typeface.DEFAULT, Typeface.ITALIC);
		text.setText(R.string.aehome_2ListDNPreview);

		RelativeLayout.LayoutParams paramsRelBarTitle = new RelativeLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
		paramsRelBarTitle.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
		paramsRelBarTitle.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
			paramsRelBarTitle.addRule(RelativeLayout.ALIGN_PARENT_START, RelativeLayout.TRUE);
		}
		paramsRelBarTitle.addRule(RelativeLayout.LEFT_OF, barClose.getId());
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
			paramsRelBarTitle.addRule(RelativeLayout.START_OF, barClose.getId());
		}
		paramsRelBarTitle.addRule(RelativeLayout.ALIGN_BOTTOM, barClose.getId());

		RelativeLayout.LayoutParams paramsRelBarClose = new RelativeLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
		paramsRelBarClose.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
		paramsRelBarClose.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
			paramsRelBarClose.addRule(RelativeLayout.ALIGN_PARENT_END, RelativeLayout.TRUE);
		}

		barTitle.setLayoutParams(paramsRelBarTitle);
		barClose.setLayoutParams(paramsRelBarClose);

		bar.addView(barTitle);
		bar.addView(barClose);

		LinearLayout.LayoutParams paramsRelBar = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
		bar.setLayoutParams(paramsRelBar);
		linearLayout.addView(bar);

		LinearLayout.LayoutParams paramsRelTexto = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
		text.setLayoutParams(paramsRelTexto);
		linearLayout.addView(text);
	}

	public void setLinearLayout(LinearLayout linearLayout) {
		this.linearLayout = linearLayout;
	}

	public LinearLayout getLinearLayout() {
		return linearLayout;
	}

	public void setText(TextView text) {
		this.text = text;
	}

	public TextView getText() {
		return text;
	}

	public void setBarTitle(TextView barTitle) {
		this.barTitle = barTitle;
	}

	public TextView getBarTitle() {
		return barTitle;
	}

	public void setBarClose(ImageView barClose) {
		this.barClose = barClose;
	}

	public ImageView getBarClose() {
		return barClose;
	}

	public void setBar(RelativeLayout bar) {
		this.bar = bar;
	}

	public RelativeLayout getBar() {
		return bar;
	}

	public String getTitle(){
		return barTitle.getText().toString();
	}

	public String getMessage(){
		return text.getText().toString();
	}

	public void setTitle(String title){
		barTitle.setText(title);
	}

	public void setMessage(String message){
		text.setText(message);
	}
}
