/*
 * Copyright © 2015-2018 Cledson Ferreira <cledsonitgames@gmail.com>
 * This work is free. You can redistribute it and/or modify it under the
 * terms of the Do What The Fuck You Want To Public License, Version 2,
 * as published by Sam Hocevar. See the COPYING file for more details.
*/

package br.clapps.aedit;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Scanner;
import android.view.*;
import android.content.*;
import android.widget.*;
import java.util.*;
import android.os.*;
import android.graphics.*;
import android.app.*;
import java.io.*;
import android.view.animation.*;

/**
 * Created by cledson on 01/04/16.
 */

public class AEHome extends Activity {
	public static final int HISTORY_LENGTH = 50;
	public static final String PATH_EXTERN_SDCARD = "/storage/sdcard1";
    public static final String PATH_INTERN_SDCARD = Environment.getExternalStorageDirectory().getPath();
	public static final String PATH_AEDOCS = PATH_INTERN_SDCARD + "/Documents/AEDocs";
	private static final int MAX_LINES = 10;
	private static final int MAX_COLS = 200;
	private static final int MAX_NOTES_PATIENCE = 10;
	private static final StringHistory PATH_HISTORY = new StringHistory(PATH_AEDOCS, HISTORY_LENGTH);
	private static String path=PATH_AEDOCS;
	private static String nome="";

	protected static String newBuffer;
	protected static String colors[];
	protected static Map<String, Integer> mapaCores;

    private static String[] fileList;
    private static File filesDir;
    private static boolean isOpen;
    private static AEHome current;
	private static Activity there;
	private NoteView[] notes;
    private int p = 0;

	private static boolean meudeus = false;

	protected static SharedPreferences spAEdit;

    private TextView barTitle;
    private LinearLayout list;

	private Dialog dOpenFile;
    private ListView lv_fileList;
    private TextView txt_sdCard;
	private TextView txt_infoDir;
    private EditText edit_selectFileURL;
    private boolean isSdCard=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.aehome);

		if (spAEdit == null) {
			spAEdit = getSharedPreferences("aedit", 0);
		}

        newBuffer = getString(R.string.title_newbuffer);

        barTitle = (TextView) findViewById(R.id.aehome_2BarTitle);
        list = (LinearLayout) findViewById(R.id.aehome_2List);

		if (mapaCores == null) {
			colors = getResources().getStringArray(R.array.colorNames);
			mapaCores = new HashMap<>();
            mapaCores.put(colors[0], Color.YELLOW);
            mapaCores.put(colors[1], Color.BLUE);
            mapaCores.put(colors[2], Color.WHITE);
            mapaCores.put(colors[3], 0xff008b8b);
            mapaCores.put(colors[4], Color.MAGENTA);
            mapaCores.put(colors[5], Color.BLACK);
            mapaCores.put(colors[6], Color.GREEN);
            mapaCores.put(colors[7], Color.RED);
		}

		filesDir = getFilesDir();
        updateFileList();
		File AEDirectory = new File(PATH_AEDOCS);
		if (!AEDirectory.isDirectory()) {
			if (!AEDirectory.mkdirs()) {
				path = PATH_INTERN_SDCARD;
			}
		}

		if (!spAEdit.getString("filePath", newBuffer).equals(newBuffer)
			&& spAEdit.getBoolean("autodit", true)) {
			startActivity(new Intent(AEHome.this, AEdit.class));
		}
		isOpen = true;
		current = this;
		setLabelsVisibility(spAEdit.getBoolean("showToolbarLabels", true));
		showNotes();

		if (spAEdit.getBoolean("isTheFirstTime1", true)) {
			AlertDialog.Builder dHelp = new AlertDialog.Builder(this);
			dHelp.setTitle(R.string.firstRunHelpT);
			dHelp.setMessage(R.string.firstRun1HelpM);
			dHelp.setNeutralButton(R.string.dBtClose, null);
			dHelp.show();
			SharedPreferences.Editor ed = spAEdit.edit();
			ed.putBoolean("isTheFirstTime1", false);
			ed.apply();
		}
    }

	@Override
	public void onBackPressed() {
		isOpen = false;
		current = null;
		super.onBackPressed();
	}

    protected static void updateFileList() {
        if (filesDir == null) return;
        fileList = null;
        try {
            FileReader frRecents = new FileReader(new File(filesDir.getPath() + "/recents.txt"));
            Scanner scFrRecents = new Scanner(frRecents);
            while (scFrRecents.hasNextLine()) {
                if (fileList == null) {
                    fileList = new String[1];
                    fileList[0] = scFrRecents.nextLine();
                } else {
                    String[] newFileList = new String[fileList.length + 1];
                    System.arraycopy(fileList, 0, newFileList, 0, fileList.length);
                    newFileList[fileList.length] = scFrRecents.nextLine();
                    fileList = newFileList;
                }
            }
            scFrRecents.close();
        } catch (IOException ignored) {
        }
        if (isOpen) {
            current.showNotes();
        }
    }

    protected static void addFileToRecents(String path) {
        if (!path.equals(newBuffer)) {
            if (fileList == null) {
                fileList = new String[1];
                fileList[0] = path;
            } else {
                for (int i=0; i < fileList.length; i++) {
                    if (path.equals(fileList[i])) {
                        fileList[i] = fileList[0];
                        fileList[0] = path;
                        return;
                    }
                }
                String[] newFileList = new String[fileList.length + 1];
                System.arraycopy(fileList, 0, newFileList, 1, fileList.length);
                newFileList[0] = path;
                fileList = newFileList;
            }
            saveFileRecents();
            if (isOpen) {
                current.showNotes();
            }
        }
    }

    protected static void saveFileRecents() {
        if (fileList == null) return;
        if (filesDir == null) return;
        try {
            FileOutputStream fw = new FileOutputStream(filesDir.getPath() + "/recents.txt");
            for (String aFileList : fileList) {
                fw.write((aFileList + "\n").getBytes());
            }
            fw.close();
        } catch (IOException ignored) {
        }
    }

	protected static void renomeie(String oldPath, String newPath) {
		for (int i=0; i<fileList.length; i++) {
			if (fileList[i].equals(oldPath)) {
				fileList[i] = newPath;
				saveFileRecents();
				updateFileList();
			}
		}
	}

	public void showNotes() {
		list.removeAllViews();
		if (fileList != null) {
			File firstFile = new File(fileList[0]);
			notes = new NoteView[fileList.length];
			notes[0] = new NoteView(this);
			notes[0].setTitle(FileSet.getFileTitle(firstFile.getName()));
			getFileText(notes, 0);
			if (firstFile.getPath().equals(spAEdit.getString("filePath", newBuffer))) {
				notes[0].setMessage(spAEdit.getString("tmpBuffer", ""));
			}
			notes[0].getBarClose().setOnClickListener(new View.OnClickListener(){
					@Override
					public void onClick(View v) {
						p = getViewNoteIndex(v);
						if (spAEdit.getString("filePath", newBuffer).equals(fileList[p])) {
							if (!spAEdit.getString("tmpBuffer", "").equals(getFileText(fileList[p]))) {
								AlertDialog.Builder dWarn = new AlertDialog.Builder(AEHome.this);
								dWarn.setTitle(R.string.discardQuestT);
								dWarn.setMessage(R.string.discardQuestM);
								dWarn.setPositiveButton(R.string.dBtConfirm, new DialogInterface.OnClickListener(){
										@Override
										public void onClick(DialogInterface d, int i) {
											removeText(false);
										}
									});
								dWarn.setNegativeButton(R.string.dBtCancel, null);
								dWarn.show();
								return;
							}
						}
						removeText(false);
					}
				});
			notes[0].getBarClose().setOnLongClickListener(new View.OnLongClickListener(){
					@Override
					public boolean onLongClick(View v) {
						p = getViewNoteIndex(v);
						AlertDialog.Builder dDel = new AlertDialog.Builder(AEHome.this);
						dDel.setTitle(R.string.deleteFileT);
						dDel.setMessage(String.format(getString(R.string.deleteFileM), fileList[p]));
						dDel.setPositiveButton(R.string.dBtYes, new DialogInterface.OnClickListener(){
								@Override
								public void onClick(DialogInterface d, int i) {
									removeText(true);
								}
							});
						dDel.setNegativeButton(R.string.dBtNot, null);
						dDel.show();
						return true;
					}
				});
			notes[0].getLinearLayout().setOnClickListener(new View.OnClickListener(){
					@Override
					public void onClick(View v) {
						openText(v);
					}
				});
			list.addView(notes[0].getLinearLayout());

			for (int i=1; i < fileList.length; i++) {
				File f = new File(fileList[i]);
				notes[i] = new NoteView(this);
				notes[i].setTitle(FileSet.getFileTitle(f.getName()));
				getFileText(notes, i);
				notes[i].getBarClose().setOnClickListener(new View.OnClickListener(){
						@Override
						public void onClick(View v) {
							p = getViewNoteIndex(v);
							if (spAEdit.getString("filePath", newBuffer).equals(fileList[p])) {
								if (!spAEdit.getString("tmpBuffer", "").equals(getFileText(fileList[p]))) {
									AlertDialog.Builder dWarn = new AlertDialog.Builder(AEHome.this);
									dWarn.setTitle(R.string.discardQuestT);
									dWarn.setMessage(R.string.discardQuestM);
									dWarn.setPositiveButton(R.string.dBtConfirm, new DialogInterface.OnClickListener(){
											@Override
											public void onClick(DialogInterface d, int i) {
												removeText(false);
											}
										});
									dWarn.setNegativeButton(R.string.dBtCancel, null);
									dWarn.show();
									return;
								}
							}
							removeText(false);
						}
					});
				notes[i].getBarClose().setOnLongClickListener(new View.OnLongClickListener(){
						@Override
						public boolean onLongClick(View v) {
							p = getViewNoteIndex(v);
							AlertDialog.Builder dDel = new AlertDialog.Builder(AEHome.this);
							dDel.setTitle(R.string.deleteFileT);
							dDel.setMessage(String.format(getString(R.string.deleteFileM), fileList[p]));
							dDel.setPositiveButton(R.string.dBtYes, new DialogInterface.OnClickListener(){
									@Override
									public void onClick(DialogInterface d, int i) {
										removeText(true);
									}
								});
							dDel.setNegativeButton(R.string.dBtNot, null);
							dDel.show();
							return true;
						}
					});
				notes[i].getLinearLayout().setOnClickListener(new View.OnClickListener(){
						@Override
						public void onClick(View v) {
							openText(v);
						}
					});
				list.addView(notes[i].getLinearLayout());
			}

			barTitle.setText(String.format(getString(R.string.aehome_2BarTitleWithNotes), fileList.length));
        } else {
			notes = new NoteView[1];
			notes[0] = new NoteView(this);
			String msg = spAEdit.getString("tmpBuffer", "");
			notes[0].setMessage(msg.equals("") ? getString(R.string.aehome_2ListDNPreview) : msg);
			notes[0].getLinearLayout().setOnClickListener(new View.OnClickListener(){
					@Override
					public void onClick(View v) {
						goAEdit(v);
					}
				});
			notes[0].getBarClose().setVisibility(View.GONE);
			list.addView(notes[0].getLinearLayout());
			barTitle.setText(R.string.aehome_2BarTitle);
		}
	}

	public void newFile(View v) {
		Intent it = new Intent(AEHome.this, AEdit.class);
		it.putExtra("filePath", "newBuffer");
		startActivity(it);
	}

	public void goAEdit(View v) {
		Intent it = new Intent(AEHome.this, AEdit.class);
		startActivity(it);
	}

	public void goAESettings(View v) {
		startActivity(new Intent(AEHome.this, AESettings.class));
	}

	public void openText(View v) {
		p = getViewNoteIndex(v);
		Intent it = new Intent(AEHome.this, AEdit.class);
		if (p >= 0 && p < notes.length) {
			if (fileList != null) if (fileList.length > 0) {
					it.putExtra("filePath", fileList[p]);
					String filePath = fileList[p];
					removeFast(p);
					addFileToRecents(filePath);
				}
		}
		startActivity(it);
		findViewById(R.id.aehome_2ScrollList).scrollTo(0, 0);
	}

	public void removeText(boolean delete) {
		if (fileList[p].equals(spAEdit.getString("filePath", ""))) {
			SharedPreferences.Editor ed = spAEdit.edit();
			ed.putString("filePath", newBuffer);
			ed.putString("tmpBuffer", "");
			ed.putInt("selectionStart", 0);
			ed.putInt("selectionEnd", 0);
			ed.apply();
		}
		if (delete) {
			if (!new File(fileList[p]).delete()) {
				Toast.makeText(this, getString(R.string.deleteFileFailed), Toast.LENGTH_SHORT).show();
				return;
			} else {
				Toast.makeText(this, getString(R.string.deleteFileSuccessfully), Toast.LENGTH_SHORT).show();
			}
		}

		Animation anim = new TranslateAnimation(0, -2000, 0, 0);
		anim.setDuration(300);
		anim.setAnimationListener(new Animation.AnimationListener() {
			public void onAnimationStart(Animation anim) {
			}
			public void onAnimationRepeat(Animation anim) {
			}
			public void onAnimationEnd(Animation anim) {
				list.removeView(notes[p].getLinearLayout());
				
				if (fileList.length > 1) {
					String[] newFileList = new String[fileList.length - 1];
					NoteView[] newNotes = new NoteView[fileList.length - 1];
					for (int z=0; z < fileList.length; z++) {
						if (z == p) {
							for (z = z + 1; z < fileList.length; z++) {
								newFileList[z - 1] = fileList[z];
								newNotes[z - 1] = notes[z];
							}
							break;
						}
						newFileList[z] = fileList[z];
						newNotes[z] = notes[z];
					}
					fileList = newFileList;
					notes = newNotes;
				} else {
					fileList = null;
					showNotes();
					if (!new File(filesDir + "/recents.txt").delete()) {
						AlertDialog.Builder dError = new AlertDialog.Builder(AEHome.this);
						dError.setTitle(R.string.fatalErrorT);
						dError.setMessage(R.string.fatalErrorM2);
						dError.setNeutralButton(R.string.dBtClose, new DialogInterface.OnClickListener(){
								@Override
								public void onClick(DialogInterface d, int i) {
									AEHome.this.onBackPressed();
								}
							});
						dError.show();
					}
					return;
				}
				if (fileList.length > 0) barTitle.setText(String.format(getString(R.string.aehome_2BarTitleWithNotes), fileList.length));
				else barTitle.setText(R.string.aehome_2BarTitle);
				saveFileRecents();
			}
		});
		notes[p].getLinearLayout().startAnimation(anim);
		// list.removeView(notes[p].getLinearLayout()); [...]
	}

	public void removeFast(int p) {
		list.removeView(notes[p].getLinearLayout());
		if (fileList.length > 1) {
			String[] newFileList = new String[fileList.length - 1];
			NoteView[] newNotes = new NoteView[fileList.length - 1];
			for (int z=0; z < fileList.length; z++) {
				if (z == p) {
					for (z = z + 1; z < fileList.length; z++) {
						newFileList[z - 1] = fileList[z];
						newNotes[z - 1] = notes[z];
					}
					break;
				}
				newFileList[z] = fileList[z];
				newNotes[z] = notes[z];
			}
			fileList = newFileList;
			notes = newNotes;
		} else {
			fileList = null;
			showNotes();
			if (!new File(filesDir + "/recents.txt").delete()) {
				AlertDialog.Builder dError = new AlertDialog.Builder(AEHome.this);
				dError.setTitle(R.string.fatalErrorT);
				dError.setMessage(R.string.fatalErrorM2);
				dError.setNeutralButton(R.string.dBtClose, new DialogInterface.OnClickListener(){
						@Override
						public void onClick(DialogInterface d, int i) {
							AEHome.this.onBackPressed();
						}
					});
				dError.show();
			}
		}
	}

	protected int getViewNoteIndex(View v) {
		for (int i=0; i < notes.length; i++) {
			if (v.equals(notes[i].getBarClose()) || v.equals(notes[i].getLinearLayout())) {
				return i;
			}
		}
		return -1;
	}

	/** Esta função configura os widgets básicos da janela de abrir arquivos (dOpenFile) */
    public void openFileDialog(View v) {
		if (dOpenFile == null) {
			dOpenFile = new Dialog(AEHome.this);
			dOpenFile.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dOpenFile.setTitle(R.string.openFileT);
			dOpenFile.setContentView(R.layout.dialog_selectfile);

			ImageView img_prevDir = (ImageView) dOpenFile.findViewById(R.id.bt_selectFilePrevDir);
			ImageView img_nextDir = (ImageView) dOpenFile.findViewById(R.id.bt_selectFileNextDir);
			txt_sdCard = (TextView) dOpenFile.findViewById(R.id.bt_selectFileSdCard);
			ImageView txt_closeBox = (ImageView) dOpenFile.findViewById(R.id.bt_selectFileCloseBox);
			txt_infoDir = (TextView) dOpenFile.findViewById(R.id.txt_selectFileInfoDir);
			edit_selectFileURL = (EditText) dOpenFile.findViewById(R.id.edit_selectFileURL);
			lv_fileList = (ListView) dOpenFile.findViewById(R.id.lv_fileList);

			listFiles(path);

			img_prevDir.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						if (!path.equals("/")) {
							if (!new File(new File(path).getParent()).canRead()) {
								errorInput();
								PATH_HISTORY.append(path);
								path = new File(PATH_INTERN_SDCARD).getPath();
								listFiles(path);
							} else {
								PATH_HISTORY.append(path);
								path = new File(path).getParent();
								listFiles(path);
							}
						}
					}
				});
			img_nextDir.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						if (PATH_HISTORY.getEnd()!=-1) {
							path = PATH_HISTORY.getEndString();
							PATH_HISTORY.remove(PATH_HISTORY.getEnd());
							listFiles(path);
						}
					}
				});
			if (!new File(PATH_EXTERN_SDCARD).isDirectory()) {
				txt_sdCard.setVisibility(View.GONE);
			} else {
				txt_sdCard.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							if (!isSdCard) {
								if (new File(PATH_EXTERN_SDCARD).isDirectory())
									path = PATH_EXTERN_SDCARD;
								else {
									errorInput();
									return;
								}
								isSdCard = true;
								txt_sdCard.setText(R.string.selectFile_Phone);
							} else {
								path = PATH_INTERN_SDCARD;
								isSdCard = false;
								txt_sdCard.setText(R.string.selectFile_SdCard);
							}
							listFiles(path);
						}
					});
			}
			txt_closeBox.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						dOpenFile.dismiss();
					}
				});

			txt_infoDir.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						txt_infoDir.setVisibility(View.GONE);
						edit_selectFileURL.setVisibility(View.VISIBLE);
					}
				});
			edit_selectFileURL.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						if (!new File(edit_selectFileURL.getText().toString()).isDirectory()
							|| !new File(edit_selectFileURL.getText().toString()).canRead()) {
							errorInput();
							return;
						}
						edit_selectFileURL.setVisibility(View.GONE);
						txt_infoDir.setVisibility(View.VISIBLE);
						PATH_HISTORY.append(path);
						path = edit_selectFileURL.getText().toString();
						listFiles(path);
					}
				});

			lv_fileList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView av, View v, int p, long id) {
						nome = (String) av.getItemAtPosition(p);
						if (!new File(path + "/" + nome).canRead()) {
							errorInput();
						} else if (new File(path + "/" + nome).isDirectory()) {
							path = new File(path + "/" + nome).getPath();
							PATH_HISTORY.clear();
							listFiles(path);
						} else if (FileSet.isTextFileName(path + "/" + nome)) {
							dOpenFile.dismiss();
							if (fileList != null) {
								if (fileList.length >= MAX_NOTES_PATIENCE && !AEHome.meudeus) {
									Toast.makeText(AEHome.this, "O.o o.O \'-\'", Toast.LENGTH_LONG).show();
									meudeus = true;
								}
							}
							Intent it = new Intent(AEHome.this, AEdit.class);
							it.putExtra("filePath", path + "/" + nome);
							startActivity(it);
							AEHome.addFileToRecents(path + "/" + nome);
						} else {
							AlertDialog.Builder erro = new AlertDialog.Builder(AEHome.this);
							erro.setTitle(R.string.fileNSupportedT);
							erro.setMessage(String.format(getString(R.string.fileNSupportedM), ("\n" + nome + "\n")));
							erro.setNeutralButton(R.string.dBtClose, null);
							erro.show();
						}
					}
				});
		}
        dOpenFile.show();
    }

	/** Lista os arquivos/pastas na janela de abrir/salvar arquivos */
    protected void listFiles(String path) {
        File dir = new File(path);
        String names[] = dir.list();
        Arrays.sort(names);

        lv_fileList.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, names));
        txt_infoDir.setText(String.format(getString(R.string.infoDir), path));
        if (path.equals(PATH_INTERN_SDCARD)) {
            txt_sdCard.setText(R.string.selectFile_SdCard);
            isSdCard = false;
        } else if (path.equals(PATH_EXTERN_SDCARD)) {
            txt_sdCard.setText(R.string.selectFile_Phone);
            isSdCard = true;
        }
		if (names.length == 0) {
			lv_fileList.setVisibility(View.INVISIBLE);
		} else {
			lv_fileList.setVisibility(View.VISIBLE);
		}
        edit_selectFileURL.setText(path);
    }

	protected static void setLabelsVisibility(boolean show) {
		if (!isOpen) return;
		TableRow barMenuRow2 = (TableRow) current.findViewById(R.id.aehome_2BarMenuRow2);
		if (show) {
			barMenuRow2.setVisibility(View.VISIBLE);
		} else {
			barMenuRow2.setVisibility(View.GONE);
		}
	}

	public static void getFileText(NoteView[] n, int p) {
		if (!isOpen) return;

		String txt="";
		if (FileSet.isAEFile(fileList[p])) {
			try {
				InputStream is = current.getAssets().open(fileList[p]);
				Scanner scIs = new Scanner(is);
				boolean hasAppliedCodeFeatures = false;
                for (int i=0; scIs.hasNext(); i++) {
                    String line = scIs.nextLine();
                    if (!hasAppliedCodeFeatures) {
						if (!current.applyCodeFeatures(line, n[p])) {
							txt += line + "\n";
							hasAppliedCodeFeatures = true;
						}
					} else {
						if (i == 0) hasAppliedCodeFeatures = true;
						txt += line + "\n";
					}
                }
                scIs.close();
			} catch (IOException e) {
				errorFatal(current);
			}
		} else {
			try {
				FileReader fr = new FileReader(new File(fileList[p]));
				Scanner sFr = new Scanner(fr);
				int lines=0, cols=0;
				boolean hasAppliedCodeFeatures=false;
				for (int i=0; sFr.hasNextLine() && lines < MAX_LINES && cols < MAX_COLS; i++) {
					String line = sFr.nextLine();
					if (!hasAppliedCodeFeatures) {
						if (!current.applyCodeFeatures(line, n[p])) {
							txt += line + "\n";
							lines++;
							cols += line.length() + 1;
							hasAppliedCodeFeatures = true;
						}
					} else {
						if (i == 0) hasAppliedCodeFeatures = true;
						txt += line + "\n";
						lines++;
						cols += line.length() + 1;
					}
				}
				txt += "… " + current.getString(R.string.previewNoteAccessibility);
				sFr.close();
			} catch (IOException e) {
				txt = current.getString(R.string.fileNFoundM);
			}
		}
		n[p].setMessage(txt);
	}

	public static String getFileText(String endereco) {
		if (!isOpen) return "";

		String txt = "";
		if (FileSet.isAEFile(endereco)) {
			try {
				InputStream is = AEHome.current.getAssets().open(endereco);
				Scanner scIs = new Scanner(is);
				String line;
				boolean hasAppliedCodeFeatures = false;
				for (int i=0; scIs.hasNextLine(); i++) {
					line = scIs.nextLine();
					if (!hasAppliedCodeFeatures) {
						if (!current.applyCodeFeatures(line, null)) {
							txt += line + "\n";
							hasAppliedCodeFeatures = true;
						}
					} else {
						if (i == 0) hasAppliedCodeFeatures = true;
						txt += line + "\n";
					}
				}
				scIs.close();
			} catch (IOException ex) {
				current.errorInput();
			}
			if (txt.length() > 0) {
				txt = txt.substring(0, txt.length() - 1);
			}
		} else {
			try {
				FileReader fr = new FileReader(new File(endereco));
				Scanner scFr = new Scanner(fr);
				String line;
				boolean hasAppliedCodeFeatures = false;
				for (int i=0; scFr.hasNextLine(); i++) {
					line = scFr.nextLine();
					if (!hasAppliedCodeFeatures) {
						if (!current.applyCodeFeatures(line, null)) {
							txt += line + "\n";
							hasAppliedCodeFeatures = true;
						}
					} else {
						if (i == 0) hasAppliedCodeFeatures = true;
						txt += line + "\n";
					}
				}
				scFr.close();
			} catch (FileNotFoundException ex) {
				current.errorInput();
			}
			if (txt.length() > 0) {
				txt = txt.substring(0, txt.length() - 1);
			}
		}

		return txt;
	}

	private boolean applyCodeFeatures(String line, NoteView nV) {
		boolean aplicado=true;
		if (line.length() < 4) return false;

		String prop = line.substring(0, 2);
		String value = line.substring(2, line.length());
		switch (prop) {
			case "th":
				switch (value) {
					case "Black":
						if (nV != null) {
							nV.getBar().setBackgroundColor(Color.BLACK);
							nV.getBarTitle().setTextColor(Color.rgb(122, 255, 122));
							nV.getText().setTextColor(Color.WHITE);
							nV.getText().setBackgroundColor(Color.rgb(0, 122, 0));
						}
						break;
					case "Light":
						if (nV != null) {
							nV.getBar().setBackgroundColor(0xffd1f3ff);
							nV.getBarTitle().setTextColor(Color.RED);
							nV.getText().setTextColor(Color.BLUE);
							nV.getText().setBackgroundColor(0xfff0f0f0);
						}
						break;
					case "Red":
						if (nV != null) {
							nV.getBar().setBackgroundColor(0xfffb4747);
							nV.getBarTitle().setTextColor(Color.WHITE);
							nV.getText().setTextColor(Color.RED);
							nV.getText().setBackgroundColor(0xfff0f0f0);
						}
						break;
					default:
						if (nV != null) {
							nV.getBar().setBackgroundColor(0xfff1b100);
							nV.getBarTitle().setTextColor(Color.WHITE);
							nV.getText().setTextColor(Color.BLUE);
							//nV.getText().setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.ITALIC));
							nV.getText().setBackgroundColor(0xfff0f0f0);
						}
				}
				break;
			case "tf":
				switch (value) {
					case "Serif":
						if (nV != null) nV.getText().setTypeface(Typeface.create(Typeface.SERIF, Typeface.ITALIC));
						break;
					case "Mono":
						if (nV != null) nV.getText().setTypeface(Typeface.create(Typeface.MONOSPACE, Typeface.ITALIC));
						break;
					default:
						if (nV != null) nV.getText().setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.ITALIC));
				}
				break;
			case "tc":
				if (nV != null) nV.getText().setTextColor(AEHome.mapaCores.get(value));
				break;
			case "tz":
				break;
			default:
				aplicado = false;
		}
		return aplicado;
	}

	/** Apresenta erro de entrada sobre tentar ler de um arquivo/pasta */
    protected void errorInput() {
        AlertDialog.Builder d = new AlertDialog.Builder(AEHome.this);
        d.setTitle(R.string.fileNFoundT);
        d.setMessage(R.string.fileNFoundM);
        d.setNeutralButton(R.string.dBtClose, null);
        d.show();
    }

	/** Apresenta erro fatal */
	protected static void errorFatal(Activity there) {
		AEHome.there = there;
		AlertDialog.Builder dFatalError = new AlertDialog.Builder(there);
		dFatalError.setTitle(R.string.fatalErrorT);
		dFatalError.setMessage(R.string.fatalErrorM);
		dFatalError.setNeutralButton(R.string.dBtClose, new DialogInterface.OnClickListener(){
				@Override
				public void onClick(DialogInterface d, int i) {
					if (current.equals(AEHome.there))
						current.onBackPressed();
					else {
						AEHome.there.onBackPressed();
						current.onBackPressed();
					}
				}
			});
		dFatalError.show();
	}
}
