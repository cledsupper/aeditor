/*
 * Copyright © 2015-2018 Cledson Ferreira <cledsonitgames@gmail.com>
 * This work is free. You can redistribute it and/or modify it under the
 * terms of the Do What The Fuck You Want To Public License, Version 2,
 * as published by Sam Hocevar. See the COPYING file for more details.
*/

package br.clapps.aedit;

import android.annotation.SuppressLint;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.*;
import android.content.DialogInterface;
import android.content.SharedPreferences;

import java.io.*;
import java.util.*;

import android.graphics.Color;
import android.text.TextWatcher;
import android.text.Editable;
import android.content.*;

import android.view.View.*;
import android.view.*;
import android.os.*;
import java.net.*;
import android.app.ActivityManager;
import android.graphics.*;
import android.view.animation.*;

/*
 * by cleds'upper (Cledson Ferreira)
 */

public class AEdit extends Activity {
    // Algumas localizações inalteráveis (ou alteráveis dependendo da versão do sistema)
    public static final String ASSET_TUTORIAL_TXT = "TUTORIAL.txt";
	public static final String ASSET_CONTRIBUIDORES_TXT = "CONTRIBUIDORES.txt";
	public static final String[] CUSTOM_NAMES = new String[]{"th", "tf", "tz", "tc"};
	public static final String CT_DEFAULT = "DEFAULT";
	public static final String NAME_THEME_DEFAULT = "Æ";
	public static final String NAME_THEME_RED = "RED";
	public static final String NAME_THEME_DARK = "DARK";
	public static final String NAME_THEME_BLACK = "BLACK";
	public static final String NAME_THEME_LIGHT = "LIGHT";

	private static final StringHistory PATH_HISTORY = new StringHistory(AEHome.PATH_AEDOCS, AEHome.HISTORY_LENGTH);
    private static final String TYPEFACES[] = {"Normal", "Serif", "Monospace"};
    private static final String SIZES[] = {"8", "10", "11", "12", "14", "18", "20", "24", "28", "36", "42", "48"};
    private static final int DEFAULT_TYPEFACE = 0;
    private static final int DEFAULT_SIZE = 5;
    private static final int DEFAULT_COLOR = 3;
	private static final double DEFAULT_ALPHA = 0.7;

	private StringHistory buffer = new StringHistory("", AEHome.HISTORY_LENGTH);
	private static StringHistory backup_buffer;
	private static String path = Environment.getExternalStorageDirectory().getPath() + "/Documents/AEDocs";

	private static boolean keepChanges = true;
	private static boolean autoSave = true;

    // Widgets da tela principal
	private int colorPrimary = 0xfff1b100;
    private RelativeLayout lay_second;
    private TableLayout barMenu;
	private TableRow barMenuRow2;
	private Button barMenuSelecttheme;
    private LinearLayout lay_third;
    private FrameLayout lay_frame;
    private TextView txt_appTitle;
	private TextView txt_statusBar;
    private EditText edit_appTitle;
	private EditText edit_textFile;

    /* Diversas variáveis utilizadas pela atividade */
    private TextView txt_infoDir;
    private boolean directlyEditted = true;
	private String filePath;
	private static String backup_filePath;
	private String past = "";
	private static String backup_past;
	private boolean editado = false;

    // Esquema de cores do aplicativo e fonte do texto
    private String tf, tz, tc, th;
	private static String backup_th, backup_tf, backup_tz, backup_tc;

	// Para o gerenciamento de tarefas
	private static boolean isOpen = false;

    // Janela de abrir/salvar arquivos (dOpenFile)
    private Dialog dOpenFile;
    private ListView lv_fileList;
    private TextView txt_sdCard;
    private EditText edit_fileName, edit_selectFileURL;

	// Janela de seleção de tema
	private Dialog dSelectTheme;

    // Janela de ajuda
    private Dialog dAbout;

    // Componentes da toolbox (table_tool)
    private ImageView toolBarTextClose;
    private ImageView toolBarTextUndo, toolBarTextRedo;
    private Spinner toolBarTextFont, toolBarTextSize, toolBarTextColor;

    // Nome do arquivo selecionado na lista e verificação da seleção da memória externa/interna
    private String nome = "";
    private boolean isSdCard = false;


    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.aedit);
		if (AEHome.spAEdit == null) {
			AEHome.spAEdit = getSharedPreferences("aedit", 0);
		}
		if (AEHome.newBuffer == null) {
			AEHome.newBuffer = getString(R.string.title_newbuffer);
		}
        if (filePath == null) {
            if (backup_filePath == null) filePath = AEHome.spAEdit.getString("filePath", AEHome.newBuffer);
			else filePath = backup_filePath;
        }
		if (AEHome.mapaCores == null) {
			AEHome.colors = getResources().getStringArray(R.array.colorNames);
			AEHome.mapaCores = new HashMap<>();
            AEHome.mapaCores.put(AEHome.colors[0], Color.YELLOW);
            AEHome.mapaCores.put(AEHome.colors[1], Color.BLUE);
            AEHome.mapaCores.put(AEHome.colors[2], Color.WHITE);
            AEHome.mapaCores.put(AEHome.colors[3], 0xff008b8b);
            AEHome.mapaCores.put(AEHome.colors[4], Color.MAGENTA);
            AEHome.mapaCores.put(AEHome.colors[5], Color.BLACK);
            AEHome.mapaCores.put(AEHome.colors[6], Color.GREEN);
            AEHome.mapaCores.put(AEHome.colors[7], Color.RED);
		}

        lay_second = (RelativeLayout) findViewById(R.id.lay_second);
        barMenu = (TableLayout) findViewById(R.id.table_tool);
		barMenuRow2 = (TableRow) findViewById(R.id.aeditBarMenuRow2);
        lay_frame = (FrameLayout) findViewById(R.id.lay_frame);
        lay_third = (LinearLayout) findViewById(R.id.lay_third);

		edit_appTitle = (EditText) findViewById(R.id.edit_apptitle);
        txt_appTitle = (TextView) findViewById(R.id.txt_apptitle);
        txt_statusBar = (TextView) findViewById(R.id.txt_statusbar);
        edit_textFile = (EditText) findViewById(R.id.edit_textfile);
        edit_textFile.addTextChangedListener(textFileWatcher);

        ImageView barMenuMinimize = (ImageView) findViewById(R.id.aeditBarMenuMinimize);
        ImageView barMenuSave = (ImageView) findViewById(R.id.aeditBarMenuSave);
        ImageView barMenuNew = (ImageView) findViewById(R.id.aeditBarMenuNew);
		barMenuSelecttheme = (Button) findViewById(R.id.aeditBarMenuSelecttheme);

        toolBarTextClose = (ImageView) findViewById(R.id.tool_txtClose);
        toolBarTextFont = (Spinner) findViewById(R.id.tool_spFont);
        toolBarTextFont.setAdapter(new ArrayAdapter<>(AEdit.this, android.R.layout.simple_spinner_dropdown_item, TYPEFACES));
		toolBarTextFont.setSelection(0);
        toolBarTextSize = (Spinner) findViewById(R.id.tool_spSize);
        toolBarTextSize.setAdapter(new ArrayAdapter<>(AEdit.this, android.R.layout.simple_spinner_dropdown_item, SIZES));
        toolBarTextSize.setSelection(DEFAULT_SIZE);
        toolBarTextColor = (Spinner) findViewById(R.id.tool_spColor);
        toolBarTextColor.setAdapter(new ArrayAdapter<>(AEdit.this, android.R.layout.simple_spinner_dropdown_item, AEHome.colors));
        toolBarTextColor.setSelection(DEFAULT_COLOR);
        toolBarTextUndo = (ImageView) findViewById(R.id.tool_btUndo);
        toolBarTextRedo = (ImageView) findViewById(R.id.tool_btRedo);

        setToolboxVisibility(false);

        txt_appTitle.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					if (applyCodeFeatures(edit_textFile.getText().toString())) {
						char[] str = edit_textFile.getText().toString().toCharArray();
						for (int i = 0; i < str.length; i++) {
							if (str[i] == '\n') {
								edit_textFile.setText(
                                    edit_textFile.getText().toString().substring(i,
																				 edit_textFile.getText().toString().length()));
								break;
							}
						}
					}
					if (barMenu.getVisibility() == View.GONE) {
						setToolboxVisibility(true);
					}
					edit_appTitle.setText(FileSet.getFileTitle(new File(filePath).getName()));
					edit_appTitle.setVisibility(View.VISIBLE);
					txt_appTitle.setVisibility(View.GONE);
				}
			});
        txt_appTitle.setOnLongClickListener(new View.OnLongClickListener() {
				@Override
				public boolean onLongClick(View v) {
					Toast.makeText(AEdit.this, R.string.titleAccessibility, Toast.LENGTH_LONG).show();
					return true;
				}
			});

		barMenuSelecttheme.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					showSelectTheme();
				}
			});

        barMenuNew.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (editado) {
						AlertDialog.Builder d = new AlertDialog.Builder(AEdit.this);
						d.setTitle(R.string.discardQuestT);
						d.setMessage(R.string.discardQuestM);
						d.setPositiveButton(R.string.dBtConfirm, new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface d, int i) {
									newBuffer();
								}
							});
						d.setNeutralButton(R.string.dBtCancel, null);
						d.show();
					} else newBuffer();
				}
			});
        barMenuNew.setOnLongClickListener(new View.OnLongClickListener() {
				@Override
				public boolean onLongClick(View v) {
					Toast.makeText(AEdit.this, R.string.newFileAccessibility, Toast.LENGTH_SHORT).show();
					return true;
				}
			});

        barMenuSave.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (!filePath.equals(ASSET_TUTORIAL_TXT) && !filePath.equals(ASSET_CONTRIBUIDORES_TXT)) {
						showSaveAsDialog();
						edit_textFile.requestFocus();
					}
				}
			});
        barMenuSave.setOnLongClickListener(new View.OnLongClickListener() {
				@Override
				public boolean onLongClick(View v) {
					Toast.makeText(AEdit.this, R.string.saveFileAccessibility, Toast.LENGTH_SHORT).show();
					return true;
				}
			});

        barMenuMinimize.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					onBackPressed();
				}
			});
        barMenuMinimize.setOnLongClickListener(new View.OnLongClickListener() {
				@Override
				public boolean onLongClick(View v) {
					Toast.makeText(AEdit.this, R.string.openFileAccessibility, Toast.LENGTH_SHORT).show();
					return true;
				}
			});

		// TODO: implementar algorítmo para a atualização do status de "editado"
        toolBarTextFont.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
				@Override
				public void onNothingSelected(AdapterView adapter) {
				}

				@Override
				public void onItemSelected(AdapterView adapter, View v, int p, long id) {
					switch (p) {
						case 1:
							edit_textFile.setTypeface(Typeface.SERIF);
							if (barMenu.isShown()) tf = "tfSerif";
							break;
						case 2:
							edit_textFile.setTypeface(Typeface.MONOSPACE);
							if (barMenu.isShown()) tf = "tfMono";
							break;
						default:
							edit_textFile.setTypeface(Typeface.DEFAULT);
							if (barMenu.isShown()) tf = null;
					}
				}
			});

        toolBarTextSize.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
				@Override
				public void onNothingSelected(AdapterView adapter) {
				}

				@Override
				public void onItemSelected(AdapterView adapter, View v, int p, long id) {
					String size = (String) adapter.getSelectedItem();
					edit_textFile.setTextSize(Float.parseFloat(size));
					if (barMenu.isShown()) {
						if (p != DEFAULT_SIZE) {
							tz = "tz" + size;
						} else tz = null;
					}
				}
			});

        toolBarTextColor.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
				@Override
				public void onNothingSelected(AdapterView adapter) {
				}

				@Override
				public void onItemSelected(AdapterView adapter, View v, int p, long id) {
					String color = (String) adapter.getSelectedItem();
					edit_textFile.setTextColor(AEHome.mapaCores.get(color));
					if (barMenu.isShown()) {
						if (p != DEFAULT_COLOR) {
							tc = "tc" + color;
						} else tc = null;
					}
				}
			});

        toolBarTextUndo.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (!buffer.has()) return;
					directlyEditted = true;
					int[] selection = {edit_textFile.getSelectionStart(), edit_textFile.getSelectionEnd()};
					if (buffer.getCaret() == 0) {
						edit_textFile.setText(past);
					} else {
						buffer.moveCaretToLeft();
						edit_textFile.setText(buffer.getString());
					}
					if (selection[0] > buffer.getString().length()) {
						selection[0] = buffer.getString().length();
						selection[1] = buffer.getString().length();
					}
					edit_textFile.setSelection(selection[0], selection[1]);
				}
			});

        toolBarTextRedo.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (!buffer.has()) return;
					directlyEditted = true;
					buffer.moveCaretToRight();
					int[] selection = {edit_textFile.getSelectionStart(), edit_textFile.getSelectionEnd()};
					edit_textFile.setText(buffer.getString());
					if (selection[0] > buffer.getString().length()) {
						selection[0] = buffer.getString().length();
						selection[1] = buffer.getString().length();
					}
					edit_textFile.setSelection(selection[0], selection[1]);
				}
			});

        toolBarTextClose.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					setToolboxVisibility(false);
					txt_appTitle.setVisibility(View.VISIBLE);
					edit_appTitle.setVisibility(View.GONE);
					if (!edit_appTitle.getText().equals(txt_appTitle.getText())
						&& !filePath.equals(AEHome.newBuffer)
						&& !FileSet.isAEFile(filePath)) {
						String[] path = FileSet.split(filePath, ".");
						String ext = path[path.length - 1];
						String diretorio = new File(filePath).getParent();
						File novoArquivo = new File(diretorio + '/' + edit_appTitle.getText().toString() + '.' + ext);
						if (new File(filePath).renameTo(novoArquivo)) {
							AEHome.renomeie(filePath, novoArquivo.getPath());
							filePath = novoArquivo.getPath();
							txt_appTitle.setText(edit_appTitle.getText());
							applyIdealTitle();
							if (keepChanges) {
								SharedPreferences.Editor ed = AEHome.spAEdit.edit();
								ed.putString("filePath", filePath);
								ed.apply();
							}
						} else {
							errorRename();
						}
					}
				}
			});
        toolBarTextClose.setOnLongClickListener(new View.OnLongClickListener() {
				@Override
				public boolean onLongClick(View v) {
					Toast.makeText(AEdit.this, R.string.closeToolBoxAccessibility, Toast.LENGTH_LONG).show();
					return true;
				}
			});

		String oldFilePath = filePath;
		String tmp = AEHome.spAEdit.getString("tmpBuffer", "");
		if (!oldFilePath.equals(AEHome.newBuffer) && !oldFilePath.isEmpty())
			openTextFile(oldFilePath, false, true);
		doRestore(tmp);
		applyIdealTitle();

		keepChanges = AEHome.spAEdit.getBoolean("keepChanges", true);

		if (AEHome.spAEdit.getBoolean("isTheFirstTime2", true)) {
			AlertDialog.Builder dHelp = new AlertDialog.Builder(this);
			dHelp.setTitle(R.string.firstRunHelpT);
			dHelp.setMessage(R.string.firstRun2HelpM);
			dHelp.setNeutralButton(R.string.dBtClose, null);
			dHelp.show();
			SharedPreferences.Editor ed = AEHome.spAEdit.edit();
			ed.putBoolean("isTheFirstTime2", false);
			ed.apply();
		}

		if (!isOpen) {
			Intent it = getIntent();
			String action = it.getAction();
			if (action != null) {
				if (action.equals(Intent.ACTION_VIEW) || action.equals(Intent.ACTION_EDIT)) {
					try {
						filePath = new File(new URI(it.getData().toString())).getPath();
					} catch (URISyntaxException ex) {
						errorInput();
						this.filePath = oldFilePath;
					}
				}
			} else if (it.hasExtra("filePath")) {
				filePath = it.getStringExtra("filePath");
			}
			if (!oldFilePath.equals(filePath)) {
				if (!tmp.equals(past)) {
					AlertDialog.Builder d = new AlertDialog.Builder(AEdit.this);
					d.setTitle(R.string.discardQuestT);
					d.setMessage(R.string.discardQuestM);
					d.setPositiveButton(R.string.dBtConfirm, new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface d, int i) {
								openTextFile(filePath, keepChanges, true);
							}
						});
					d.setNegativeButton(R.string.dBtCancel, new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface d, int i) {
								filePath = AEHome.spAEdit.getString("filePath", AEHome.newBuffer);
							}
						});
					d.show();
				} else {
					openTextFile(filePath, keepChanges, true);
				}
			}
			isOpen = true;
		}
    }

	@Override
	public void onResume() {
		super.onResume();
		keepChanges = AEHome.spAEdit.getBoolean("keepChanges", true);
		autoSave = AEHome.spAEdit.getBoolean("autoSave", false);
		setLabelsVisibility(AEHome.spAEdit.getBoolean("showToolbarLabels", true));
	}

    /**
     * Salva o conteúdo na memória antes de fechar a tela
     */
    @Override
    public void onBackPressed() {
		if (isOpen) {
			doBackup();
			isOpen = !keepChanges;
		}
		if (autoSave && editado && !filePath.equals(AEHome.newBuffer)) {
			if (saveTextFile(filePath)) Toast.makeText(this, R.string.fileAutoSavedToast, Toast.LENGTH_SHORT).show();
		} else if (isOpen && editado) {
			AlertDialog.Builder dWarn = new AlertDialog.Builder(this);
			dWarn.setTitle(R.string.discardQuestT);
			dWarn.setMessage(R.string.discardQuestM);
			dWarn.setPositiveButton(R.string.dBtConfirm, new DialogInterface.OnClickListener(){
					@Override
					public void onClick(DialogInterface d, int i) {
						isOpen = false;
						AEdit.this.onBackPressed();
					}
				});
			dWarn.setNegativeButton(R.string.dBtCancel, null);
			dWarn.show();
			return;
		}
		AEHome.updateFileList();
		setTheme(R.style.AppTheme);
		backup_filePath = null;
		backup_buffer = null;
		backup_past = null;
		backup_th = null;
		backup_tf = null;
		backup_tz = null;
		backup_tc = null;
		isOpen = false;
        super.onBackPressed();
    }

	@Override
	public void onPause() {
		if (isOpen) doBackup();
		super.onPause();
	}

    /**
     * Atualiza as barras de título e de status
     */
    @SuppressLint("SetTextI18n")
    protected void applyIdealTitle() {
        File arquivo = new File(filePath);
        String nomeArquivo = arquivo.getName();
        String titulo = (editado ? " * " : "") + FileSet.getFileTitle(nomeArquivo);
        txt_appTitle.setText(titulo);
        String filePath;
        if (this.filePath.length() > 25) {
            filePath = "…/" + arquivo.getParentFile().getName() + "/" + nomeArquivo;
        } else filePath = this.filePath;
        txt_statusBar.setText(filePath + " | " + edit_textFile.getText().length() + " | "
							  + getString(R.string.app_name));
    }

    @SuppressLint("SetTextI18n")
    protected void applyIdealTitle(int length) {
        File arquivo = new File(filePath);
        String nomeArquivo = arquivo.getName();
        String titulo = (editado ? " * " : "") + FileSet.getFileTitle(nomeArquivo);
        txt_appTitle.setText(titulo);
        String filePath;
        if (this.filePath.length() > 25) {
            filePath = "…/" + arquivo.getParentFile().getName() + "/" + nomeArquivo;
        } else filePath = this.filePath;
        txt_statusBar.setText(filePath + " | " + length + " | "
							  + getString(R.string.app_name));
    }

    /**
     * Inicia uma nova instância
     */
    protected void newBuffer() {
        filePath = AEHome.newBuffer;
        edit_textFile.setText(past = "");
        editado = false;
        applyIdealTitle();
        applyDefaultTheme();
        buffer.clear();
        buffer.append("");
        SharedPreferences.Editor ed = AEHome.spAEdit.edit();
        ed.putString("filePath", filePath);
        ed.putString("tmpBuffer", past);
		ed.putInt("selectionStart", 0);
		ed.putInt("selectionEnd", 0);
		th = tf = tz = tc = null;
		for (String custom_name : CUSTOM_NAMES) {
			ed.putString(custom_name, CT_DEFAULT);
		}
        ed.apply();
    }

	public void doBackup() {
		backup_buffer = buffer;
		backup_filePath = filePath;
		backup_past = past;
		backup_th = th;
		backup_tf = tf;
		backup_tz = tz;
		backup_tc = tc;
		if (keepChanges) {
			SharedPreferences.Editor ed = AEHome.spAEdit.edit();
			if (th != null) ed.putString(CUSTOM_NAMES[0], th);
			else ed.putString(CUSTOM_NAMES[0], CT_DEFAULT);
			if (tf != null) ed.putString(CUSTOM_NAMES[1], tf);
			else ed.putString(CUSTOM_NAMES[1], CT_DEFAULT);
			if (tz != null) ed.putString(CUSTOM_NAMES[2], tz);
			else ed.putString(CUSTOM_NAMES[2], CT_DEFAULT);
			if (tc != null) ed.putString(CUSTOM_NAMES[3], tc);
			else ed.putString(CUSTOM_NAMES[3], CT_DEFAULT);
			ed.putString("tmpBuffer", edit_textFile.getText().toString());
			ed.putString("filePath", this.filePath);
			ed.putInt("selectionStart", edit_textFile.getSelectionStart());
			ed.putInt("selectionEnd", edit_textFile.getSelectionEnd());
			ed.apply();
		}
	}

	public void doRestore(String tmp) {
		if (backup_buffer != null) {
			buffer = backup_buffer;
			th = backup_th;
			tf = backup_tf;
			tz = backup_tz;
			tc = backup_tc;
			if (th != null) if (!th.equals(CT_DEFAULT)) applyCodeFeatures(th);
			if (tf != null) if (!tf.equals(CT_DEFAULT)) applyCodeFeatures(tf);
			if (tz != null) if (!tz.equals(CT_DEFAULT)) applyCodeFeatures(tz);
			if (tc != null) if (!tc.equals(CT_DEFAULT)) applyCodeFeatures(tc);
		}
		if (keepChanges) {
			String[] custom_names = new String[]{"th", "tf", "tc", "tz"};
			for (String custom_name : custom_names) {
				String ct = AEHome.spAEdit.getString(custom_name, CT_DEFAULT);
				if (!ct.equals(CT_DEFAULT)) applyCodeFeatures(ct);
			}
			edit_textFile.setText(tmp);
			int selectionStart = AEHome.spAEdit.getInt("selectionStart", 0);
			int selectionEnd = AEHome.spAEdit.getInt("selectionEnd", 0);
			if (selectionEnd > tmp.length()) selectionStart = selectionEnd = 0;
			edit_textFile.setSelection(selectionStart, selectionEnd);
			if (backup_buffer != null) buffer = backup_buffer;
		}
	}

    protected void applyDefaultTheme() {
		edit_appTitle.setTextColor(Color.WHITE);
        txt_appTitle.setTextColor(Color.WHITE);
		barMenuSelecttheme.setTextColor(Color.WHITE);
		TableRow row2 = (TableRow) findViewById(R.id.aeditBarMenuRow2);
		for (int i=0; i < row2.getChildCount(); i++) {
			((TextView)row2.getChildAt(i)).setTextColor(Color.WHITE);
		}

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			txt_statusBar.setAlpha((float)DEFAULT_ALPHA);
		}
		edit_textFile.setTextColor(0xff008b8b);
        lay_frame.setBackgroundColor(0xfff0f0f0);
        edit_textFile.setBackgroundColor(0xfff0f0f0);
		colorPrimary = 0xfff1b100;
        lay_second.setBackgroundColor(0xfff1b100);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			Window window = getWindow();
			window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
			window.setStatusBarColor(0xff8f7000);
		}
		setTheme(R.style.AppTheme);
        lay_third.setBackgroundColor(0x00000000);
        toolBarTextFont.setSelection(DEFAULT_TYPEFACE);
        toolBarTextSize.setSelection(DEFAULT_SIZE);
		toolBarTextColor.setSelection(DEFAULT_COLOR); // ciano escuro
		edit_textFile.setTypeface(Typeface.DEFAULT);
		edit_textFile.setTextSize(Float.parseFloat(SIZES[DEFAULT_SIZE]));
        th = tf = tz = tc = null;
		barMenuSelecttheme.setText(NAME_THEME_DEFAULT);
    }

	private void applyBlackTheme() {
		edit_appTitle.setTextColor(Color.rgb(122, 222, 122));
		txt_appTitle.setTextColor(Color.rgb(122, 222, 122));
		barMenuSelecttheme.setTextColor(Color.rgb(122, 222, 122));
		for (int i=0; i < barMenuRow2.getChildCount(); i++) {
			((TextView)barMenuRow2.getChildAt(i)).setTextColor(Color.WHITE);
		}

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			txt_statusBar.setAlpha(1);
		}
		edit_textFile.setTextColor(Color.WHITE);
		toolBarTextColor.setSelection(2); // branco
		edit_textFile.setBackgroundColor(Color.rgb(0, 122, 0));
		lay_frame.setBackgroundColor(Color.rgb(0, 122, 0));
		colorPrimary = Color.BLACK;
		lay_second.setBackgroundColor(Color.BLACK);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			Window window = getWindow();
			window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
			window.setStatusBarColor(Color.BLACK);
		}
		setTheme(R.style.AppTheme_Black);
		lay_third.setBackgroundColor(Color.BLACK);
		th = "thBlack";
		barMenuSelecttheme.setText(NAME_THEME_BLACK);
	}

	protected void applyRedTheme() {
		edit_appTitle.setTextColor(Color.WHITE);
		txt_appTitle.setTextColor(Color.WHITE);
		barMenuSelecttheme.setTextColor(Color.WHITE);
		for (int i=0; i < barMenuRow2.getChildCount(); i++) {
			((TextView)barMenuRow2.getChildAt(i)).setTextColor(Color.WHITE);
		}

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			txt_statusBar.setAlpha((float)DEFAULT_ALPHA);
		}
		edit_textFile.setTextColor(Color.RED);
		toolBarTextColor.setSelection(7); // vermelho
		lay_frame.setBackgroundColor(0xfff0f0f0);
        edit_textFile.setBackgroundColor(0xfff0f0f0);
		colorPrimary = 0xfffb4747;
		lay_second.setBackgroundColor(0xfffb4747);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			Window window = getWindow();
			window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
			window.setStatusBarColor(0xff8f0000);
		}
		setTheme(R.style.AppTheme_Red);
		lay_third.setBackgroundColor(0x00000000);
		th = "thRed";
		barMenuSelecttheme.setText(NAME_THEME_RED);
	}

	protected void applyLightTheme() {
		edit_appTitle.setTextColor(Color.RED);
		txt_appTitle.setTextColor(Color.RED);
		barMenuSelecttheme.setTextColor(Color.RED);
		for (int i=0; i < barMenuRow2.getChildCount(); i++) {
			((TextView)barMenuRow2.getChildAt(i)).setTextColor(Color.RED);
		}

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			txt_statusBar.setAlpha((float)DEFAULT_ALPHA);
		}
		edit_textFile.setTextColor(Color.BLUE);
		toolBarTextColor.setSelection(1); // azul
		lay_frame.setBackgroundColor(0xfff0f0f0);
        edit_textFile.setBackgroundColor(0xfff0f0f0);
		colorPrimary = 0xffd1f3ff;
		lay_second.setBackgroundColor(0xffd1f3ff);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			Window window = getWindow();
			window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
			window.setStatusBarColor(0xff66abc4);
		}
		setTheme(R.style.AppTheme_Light);
		lay_third.setBackgroundColor(0xff8fbdcd);
		th = "thLight";
		barMenuSelecttheme.setText(NAME_THEME_LIGHT);
	}

	protected void showSelectTheme() {
		if (dSelectTheme == null) {
			dSelectTheme = new Dialog(this);
			dSelectTheme.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dSelectTheme.setContentView(R.layout.dialog_selecttheme);

			Button AE = (Button) dSelectTheme.findViewById(R.id.dialog_selectthemeBtDefaultTheme);
			Button Red = (Button) dSelectTheme.findViewById(R.id.dialog_selectthemeBtRedTheme);
			Button Black = (Button) dSelectTheme.findViewById(R.id.dialog_selectthemeBtHMXTheme);
			Button Light = (Button) dSelectTheme.findViewById(R.id.dialog_selectthemeBtLightTheme);

			AE.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						applyDefaultTheme();
						tornarEditado();
						dSelectTheme.dismiss();
					}
				});

			Red.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						applyRedTheme();
						tornarEditado();
						dSelectTheme.dismiss();
					}
				});

			Black.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						applyBlackTheme();
						tornarEditado();
						dSelectTheme.dismiss();
					}
				});

			Light.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						applyLightTheme();
						tornarEditado();
						dSelectTheme.dismiss();
					}
				});
		}

		dSelectTheme.show();
	}

    /**
     * Esta função configura os widgets básicos da janela de abrir/salvar arquivos (dOpenFile)
     */
    protected void configureOpenFileBasic() {
        ImageView img_prevDir = (ImageView) dOpenFile.findViewById(R.id.bt_selectFilePrevDir);
        ImageView img_nextDir = (ImageView) dOpenFile.findViewById(R.id.bt_selectFileNextDir);
        txt_sdCard = (TextView) dOpenFile.findViewById(R.id.bt_selectFileSdCard);
        ImageView img_closeBox = (ImageView) dOpenFile.findViewById(R.id.bt_selectFileCloseBox);
        txt_infoDir = (TextView) dOpenFile.findViewById(R.id.txt_selectFileInfoDir);
        edit_selectFileURL = (EditText) dOpenFile.findViewById(R.id.edit_selectFileURL);

        lv_fileList = (ListView) dOpenFile.findViewById(R.id.lv_fileList);

        listFiles(path);

        img_prevDir.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (!path.equals("/")) {
						if (!new File(new File(path).getParent()).canRead()) {
							errorInput();
							PATH_HISTORY.append(path);
							path = new File(AEHome.PATH_INTERN_SDCARD).getPath();
							listFiles(path);
						} else {
							PATH_HISTORY.append(path);
							path = new File(path).getParent();
							listFiles(path);
						}
					}
				}
			});
        img_nextDir.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (PATH_HISTORY.getEnd() != -1) {
						path = PATH_HISTORY.getEndString();
						PATH_HISTORY.remove(PATH_HISTORY.getEnd());
						listFiles(path);
					}
				}
			});
		if (!new File(AEHome.PATH_EXTERN_SDCARD).isDirectory()) {
			txt_sdCard.setVisibility(View.GONE);
		} else {
        	txt_sdCard.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						if (!isSdCard) {
							if (new File(AEHome.PATH_EXTERN_SDCARD).isDirectory())
								path = AEHome.PATH_EXTERN_SDCARD;
							else {
								errorInput();
								return;
							}
							isSdCard = true;
							txt_sdCard.setText(R.string.selectFile_Phone);
						} else {
							path = AEHome.PATH_INTERN_SDCARD;
							isSdCard = false;
							txt_sdCard.setText(R.string.selectFile_SdCard);
						}
						listFiles(path);
					}
				});
		}
        img_closeBox.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dOpenFile.dismiss();
				}
			});

        txt_infoDir.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					txt_infoDir.setVisibility(View.GONE);
					edit_selectFileURL.setVisibility(View.VISIBLE);
				}
			});
        edit_selectFileURL.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (!new File(edit_selectFileURL.getText().toString()).isDirectory()
                        || !new File(edit_selectFileURL.getText().toString()).canRead()) {
						errorInput();
						return;
					}
					edit_selectFileURL.setVisibility(View.GONE);
					txt_infoDir.setVisibility(View.VISIBLE);
					PATH_HISTORY.append(path);
					path = new File(edit_selectFileURL.getText().toString()).getPath();
					listFiles(path);
				}
			});
    }

    /**
     * Configura detalhes específicos para a janela de salvar arquivos
     */
    protected void showSaveAsDialog() {

        if (filePath.equals(AEHome.newBuffer) || filePath.isEmpty()) {
            if (dOpenFile == null) {
				dOpenFile = new Dialog(AEdit.this);
				dOpenFile.requestWindowFeature(Window.FEATURE_NO_TITLE);
				dOpenFile.setTitle(R.string.saveFileAsT);
				dOpenFile.setContentView(R.layout.dialog_selectfile);
				configureOpenFileBasic();

				ImageView img_saveIt = (ImageView) dOpenFile.findViewById(R.id.bt_selectFileSave);
				edit_fileName = (EditText) dOpenFile.findViewById(R.id.edit_selectFileName);
				dOpenFile.findViewById(R.id.ll_selectFileSaveAs).setVisibility(View.VISIBLE);

				lv_fileList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
						@Override
						public void onItemClick(AdapterView av, View v, int p, long id) {
							nome = (String) av.getItemAtPosition(p);
							String newPath = path + "/" + nome;
							if (new File(newPath).isDirectory()) {
								if (!new File(newPath).canRead()) {
									errorInput();
									return;
								}
								path = new File(newPath).getPath();
								PATH_HISTORY.clear();
								listFiles(path);
							} else {
								if (!new File(newPath).canWrite()) {
									errorOutput();
									return;
								}
								AlertDialog.Builder confirmacao = new AlertDialog.Builder(AEdit.this);
								confirmacao.setTitle(R.string.overwriteFileT);
								confirmacao.setMessage(String.format(getString(R.string.overwriteFileM), nome));
								confirmacao.setPositiveButton(R.string.dBtYes, new Dialog.OnClickListener() {
										@Override
										public void onClick(DialogInterface d, int i) {
											dOpenFile.dismiss();
											saveTextFile(path + "/" + nome);
											AEHome.addFileToRecents(path + "/" + nome);
										}
									});
								confirmacao.setNegativeButton(R.string.dBtNot, null);
								confirmacao.show();
							}
						}
					});

				img_saveIt.setOnClickListener(new View.OnClickListener() {
						public void onClick(View v) {
							String filename = edit_fileName.getText().toString();
							if (filename.isEmpty()) {
								Toast.makeText(AEdit.this, R.string.fileNameIsEmptyToast, Toast.LENGTH_SHORT).show();
								return;
							}
							if (!FileSet.isTextFileName(filename))
								filename += ".adit";

							dOpenFile.dismiss();

							String newPath = path + "/" + filename;
							if (new File(path).canWrite()) {
								saveTextFile(newPath);
								AEHome.addFileToRecents(newPath);
							} else {
								errorOutput();
							}
						}
					});
			}
            dOpenFile.show();
        } else saveTextFile(filePath);
    }

    /**
     * Lista os arquivos/pastas na janela de abrir/salvar arquivos
     */
    protected void listFiles(String path) {
        File dir = new File(path);
        String names[] = dir.list();
        Arrays.sort(names);

        lv_fileList.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, names));
        txt_infoDir.setText(String.format(getString(R.string.infoDir), path));
        if (path.equals(AEHome.PATH_INTERN_SDCARD)) {
            txt_sdCard.setText(R.string.selectFile_SdCard);
            isSdCard = false;
        } else if (path.equals(AEHome.PATH_EXTERN_SDCARD)) {
            txt_sdCard.setText(R.string.selectFile_Phone);
            isSdCard = true;
        }
		if (names.length == 0) {
			lv_fileList.setVisibility(View.INVISIBLE);
		} else {
			lv_fileList.setVisibility(View.VISIBLE);
		}
        edit_selectFileURL.setText(path);
    }

	protected void setLabelsVisibility(boolean show) {
		if (show) {
			barMenuRow2.setVisibility(View.VISIBLE);
		} else {
			barMenuRow2.setVisibility(View.GONE);
		}
	}

	protected void tornarEditado() {
		editado = true;
		applyIdealTitle();
	}

    /**
     * Abre um arquivo de texto no editor
     */
    protected void openTextFile(String endereco, boolean save, boolean resetHistory) {
        String line, s = "";

		if (filePath.equals("newBuffer")) {
			newBuffer();
			return;
		} else if (FileSet.isAEFile(endereco)) {
			applyDefaultTheme();
            try {
                InputStream is = getAssets().open(endereco);
                Scanner scIs = new Scanner(is);
				boolean hasAppliedCodeFeatures=false;
                for (int i=0; scIs.hasNext(); i++) {
                    line = scIs.nextLine();
                    if (!hasAppliedCodeFeatures) {
						if (!applyCodeFeatures(line)) {
							s += line + "\n";
							hasAppliedCodeFeatures = true;
						}
					} else {
						if (i == 0) hasAppliedCodeFeatures = true;
						s += line + "\n";
					}
                }
                scIs.close();
            } catch (IOException ex) {
                AEHome.errorFatal(this);
            }
			if (s.length() > 0) {
				past = s.substring(0, s.length() - 1);
			} else past = s;
        } else {
            if (!new File(endereco).canRead()) {
                errorInput();
                newBuffer();
                return;
            }
			applyDefaultTheme();
            try {
                FileReader fr = new FileReader(new File(endereco));
                Scanner scIs = new Scanner(fr);
				boolean hasAppliedCodeFeatures = false;
                for (int i=0; scIs.hasNext(); i++) {
                    line = scIs.nextLine();
                    if (!hasAppliedCodeFeatures) {
						if (!applyCodeFeatures(line)) {
							s += line + "\n";
							hasAppliedCodeFeatures = true;
						}
					} else {
						if (i == 0) hasAppliedCodeFeatures = true;
						s += line + "\n";
					}
                }
                scIs.close();
            } catch (FileNotFoundException ex) {
                errorInput();
            }
			if (s.length() > 0) {
				past = s.substring(0, s.length() - 1);
			} else past = s;
        }
        editado = false;
        edit_textFile.setText(past);
		if (resetHistory) {
        	buffer.clear();
        	buffer.append(past);
		}
        filePath = endereco;
		AEHome.addFileToRecents(filePath);
        if (save) {
            SharedPreferences.Editor ed = AEHome.spAEdit.edit();
            ed.putString("filePath", filePath);
            ed.putString("tmpBuffer", past);
            ed.apply();
        }
        applyIdealTitle();
    }

    /**
     * Salva o conteúdo do editor num arquivo de texto
     */
    protected boolean saveTextFile(String endereco) {
        FileOutputStream f;
		if (endereco == null) return false;
		if (endereco.equals(AEHome.newBuffer) || FileSet.isAEFile(endereco))
			return false;
        if (!new File(path).canWrite()) {
            errorOutput();
            return false;
        }

        char[] str = edit_textFile.getText().toString().toCharArray();
        for (int i = 0; i < str.length; i++) {
            if (str[i] == '\n') {
                if (edit_textFile.getText().toString().substring(0, i).equals("reset")) {
                    edit_textFile.setText(edit_textFile.getText().toString().substring(i + 1, str.length));
                    tc = th = tf = tz = null;
                    break;
                }
            }
        }

        try {
            f = new FileOutputStream(endereco);
            String ext = endereco.substring(endereco.length() - 5, endereco.length());
            if (ext.equalsIgnoreCase(".adit")) {
                if (th != null)
                    f.write((th + "\n").getBytes());
                if (tc != null)
                    f.write((tc + "\n").getBytes());
                if (tf != null)
                    f.write((tf + "\n").getBytes());
                if (tz != null)
                    f.write((tz + "\n").getBytes());
            }
            f.write(edit_textFile.getText().toString().getBytes());
            f.close();
        } catch (IOException ex) {
            errorOutput();
			return false;
        }
		SharedPreferences.Editor ed = AEHome.spAEdit.edit();
		if (keepChanges) {
			ed.putString("filePath", endereco);
			ed.putString("tmpBuffer", edit_textFile.getText().toString());
		}
		ed.putInt("selectionStart", edit_textFile.getSelectionStart());
		ed.putInt("selectionEnd", edit_textFile.getSelectionEnd());
		ed.apply();
		openTextFile(endereco, false, false);
		int[] selection = {AEHome.spAEdit.getInt("selectionStart", 0), AEHome.spAEdit.getInt("selectionEnd", 0)};
		if (selection[1] > buffer.getString().length()) {
			edit_textFile.setSelection(buffer.getString().length(), buffer.getString().length());
		} else edit_textFile.setSelection(selection[0], selection[1]);
		return true;
    }

	public void showAbout(View v) {
		dAbout = new Dialog(AEdit.this);
		dAbout.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dAbout.setContentView(R.layout.dialog_about);
		dAbout.setTitle(getString(R.string.aboutT) + " " + getString(R.string.app_name));

		dAbout.findViewById(R.id.bt_aboutClose).setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dAbout.dismiss();
				}
			});
		dAbout.findViewById(R.id.bt_aboutHelp).setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dAbout.dismiss();
					openTextFile(ASSET_TUTORIAL_TXT, keepChanges, true);
				}
			});
		dAbout.findViewById(R.id.bt_aboutSupporters).setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View v) {
					dAbout.dismiss();
					openTextFile(ASSET_CONTRIBUIDORES_TXT, keepChanges, true);
				}
			});
		dAbout.show();
	}

    /**
     * Apresenta erro de escrita sobre tentar salvar no arquivo
     */
    protected void errorOutput() {
        AlertDialog.Builder d = new AlertDialog.Builder(AEdit.this);
        d.setTitle(R.string.fileNMakedT);
        d.setMessage(R.string.fileNMakedM);
        d.setNeutralButton(R.string.dBtClose, null);
        d.show();
    }

	/**
	 * Apresenta erro ao renomear arquivo
	 */
	protected void errorRename() {
		AlertDialog.Builder d = new AlertDialog.Builder(AEdit.this);
        d.setTitle(R.string.fileNRenamedT);
        d.setMessage(R.string.fileNRenamedM);
        d.setNeutralButton(R.string.dBtClose, null);
        d.show();
	}

    /**
     * Apresenta erro de entrada sobre tentar ler de um arquivo/pasta
     */
    protected void errorInput() {
        AlertDialog.Builder d = new AlertDialog.Builder(AEdit.this);
        d.setTitle(R.string.fileNFoundT);
        d.setMessage(R.string.fileNFoundM);
        d.setNeutralButton(R.string.dBtClose, null);
        d.show();
    }

    /**
     * Aplica esquemas especiais no aplicativo de acordo com editting
     */
    protected boolean applyCodeFeatures(String editting) {
        if (editting.length() < 3) return false;

        String prop = editting.substring(0, 2);
        String valor = editting.substring(2, editting.length());

        switch (prop) {
            case "tf":
                switch (valor) {
                    case "Serif":
                        edit_textFile.setTypeface(Typeface.SERIF);
                        toolBarTextFont.setSelection(1);
                        tf = editting;
                        break;
                    case "Mono":
                        edit_textFile.setTypeface(Typeface.MONOSPACE);
                        toolBarTextFont.setSelection(2);
                        tf = editting;
                        break;
                    default:
                        edit_textFile.setTypeface(Typeface.DEFAULT);
                        toolBarTextFont.setSelection(DEFAULT_TYPEFACE);
                }
                break;
            case "tz":
                for (int i = 0; i < SIZES.length; i++) {
                    if (valor.equals(SIZES[i])) {
                        toolBarTextSize.setSelection(i);
                        edit_textFile.setTextSize(Float.parseFloat(valor));
                        tz = editting;
                    }
                }
                break;
            case "tc":
                for (int i=0; i < AEHome.colors.length; i++) {
                    if (valor.equals(AEHome.colors[i])) {
                        toolBarTextColor.setSelection(i);
                        edit_textFile.setTextColor(AEHome.mapaCores.get(valor));
                        tc = editting;
                    }
                }
                break;
            case "th":
                switch (valor) {
					case "Black":
						applyBlackTheme();
						break;
					case "Red":
						applyRedTheme();
						break;
                    case "Light":
                        applyLightTheme();
                        break;
                    default:
                        applyDefaultTheme();
                }
                break;
            default:
                return false;
        }

        return true;
    }

    private void setToolboxVisibility(boolean visible) {
        if (visible) {
			Animation anim = new TranslateAnimation(0,0,-64,0);
			anim.setDuration(250);
            barMenu.setVisibility(View.VISIBLE);
			barMenu.startAnimation(anim);
            toolBarTextClose.setVisibility(View.VISIBLE);
			toolBarTextClose.startAnimation(anim);
            toolBarTextUndo.setVisibility(View.VISIBLE);
			toolBarTextUndo.startAnimation(anim);
            toolBarTextRedo.setVisibility(View.VISIBLE);
			toolBarTextRedo.startAnimation(anim);
        } else {
            barMenu.setVisibility(View.GONE);
            toolBarTextClose.setVisibility(View.GONE);
            toolBarTextUndo.setVisibility(View.GONE);
            toolBarTextRedo.setVisibility(View.GONE);
        }
    }

    /**
     * Um TextWatcher para verificar quando o texto é alterado
     */
    final TextWatcher textFileWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence p1, int p2, int p3, int p4) {}

        @Override
        public void onTextChanged(CharSequence p1, int p2, int p3, int p4) {}

        @Override
        public void afterTextChanged(Editable p1) {
            boolean textoOriginal = p1.toString().equals(past);
            if (!editado && !textoOriginal) {
                editado = true;

            } else if (editado && textoOriginal) {
                editado = false;
            }
			if (!directlyEditted) {
				if (buffer.getCaret() < buffer.getEnd()) buffer.clearFromCaret();
				if (buffer.getEndString() != null) {
					if (buffer.getEndString().length() < p1.toString().length()) {
						buffer.append(p1.toString());
					} else if (buffer.getEndString().length() > p1.toString().length() && !directlyEditted) {
						buffer.append(p1.toString());
					}
				}
				directlyEditted = true;
			} else {
				buffer.setString(p1.toString(), buffer.getCaret());
                directlyEditted = false;
            }
            applyIdealTitle(p1.length());
        }
    };
}
