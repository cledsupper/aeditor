/*
 * Copyright © 2015-2018 Cledson Ferreira <cledsonitgames@gmail.com>
 * This work is free. You can redistribute it and/or modify it under the
 * terms of the Do What The Fuck You Want To Public License, Version 2,
 * as published by Sam Hocevar. See the COPYING file for more details.
*/

package br.clapps.aedit;
import java.util.*;

/*
 * by cleds'upper (Cledson Ferreira)
 */
public class FileSet{
	public static final String[] FORMATOS_TEXTOPURO =
		{ "adit", "btdroid", "config", "c",
			"cfg", "cpp", "css", "htm",
			"c++", "html", "ini", "desktop",
			"java", "js", "log", "lua",
			"php", "prop", "py", "md",
			"sh", "txt", "xml", "md5",
			"conf", "perl", "sh"
		};
	/**
	 * Correção da função split() da classe String para separar por qualquer termo.
	 * @param text : texto a ser separado.
	 * @param exp : expressão de divisão contida em text.
	 */
	public static String[] split(String text, String exp) {
		char[] textArray = text.toCharArray();
		char[] expArray = exp.toCharArray();
		ArrayList<String> result = new ArrayList<>();
		int start = 0, end = textArray.length;
		for (int i=0; i<textArray.length; i++) {
			if (textArray[i]==expArray[0]) {
				boolean equals = true;
				for (int z=0; z<exp.length(); z++) {
					if ((i+z)>=textArray.length) {
						equals=false;
						break;
					}
					if (textArray[i+z]!=expArray[z]) {
						equals=false;
						break;
					}
				}
				if (equals) {
					end = i;
					result.add(text.substring(start, end));
					i+=expArray.length;
					start = i;
					end = textArray.length;
					continue;
				}
			}
		}
		result.add(text.substring(start, text.length()));
		String[] resultArray = new String[result.size()];
		System.arraycopy(result.toArray(), 0, resultArray, 0, resultArray.length);
		return resultArray;
	}

	/**
	 * Retira a extensão do nome do arquivo. Para ser utilizado comp título do editor.
	 * @param fileName : nome do arquivo.
	 * @return : retorna o nome sem extensão
	 */
	public static String getFileTitle(String fileName) {
		if (fileName == null) return null;
		String title = fileName;
		int len;
		boolean hasFound=false;
		for (len = fileName.length() - 1; len > 1; len--) {
			if (fileName.charAt(len) == '.') {
				hasFound = true;
				break;
			}
		}
		if (hasFound) title = fileName.substring(0, len);
		return title;
	}

	/**
	 * Verifica pelo nome do arquivo se ele é texto puro.
	 * @param name : nome do arquivo.
	 * @return : verdadeiro se o arquivo tem extensão de texto puro.
	 */
	public static boolean isTextFileName(String name) {
		if (name==null) return false;
		if (name.isEmpty()) return false;

		String[] nameArray = split(name, ".");
		if (nameArray.length==1) return false;
		String ext = nameArray[nameArray.length-1];
		for (String thisExt : FORMATOS_TEXTOPURO) {
			if (thisExt.equals(ext)) return true;
		}
		return false;
	}

	public static boolean isAEFile(String filePath) {
		return filePath.equals(AEdit.ASSET_TUTORIAL_TXT) || filePath.equals(AEdit.ASSET_CONTRIBUIDORES_TXT);
	}
}
