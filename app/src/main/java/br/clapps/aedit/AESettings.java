/*
 * Copyright © 2015-2018 Cledson Ferreira <cledsonitgames@gmail.com>
 * This work is free. You can redistribute it and/or modify it under the
 * terms of the Do What The Fuck You Want To Public License, Version 2,
 * as published by Sam Hocevar. See the COPYING file for more details.
*/

package br.clapps.aedit;

//import android.app.Activity;
import android.os.Bundle;
import android.view.Window;
import android.view.*;
import android.widget.*;
import android.content.*;
import android.app.*;

/**
 * Created by cledson on 04/04/16.
 */
public class AESettings extends Activity {
	private TableRow barMenuRow2;
	
	private Switch keepChanges;
	private Switch autodit;
	private Switch autoSave;
	private Switch showToolBarLabels;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.aesettings);

		barMenuRow2 = (TableRow) findViewById(R.id.aesettingsbarMenuRow2);
		keepChanges = (Switch) findViewById(R.id.aesettingsKeepchanges);
		autodit = (Switch) findViewById(R.id.aesettingsAutodit);
		autoSave = (Switch) findViewById(R.id.aesettingsAutosave);
		showToolBarLabels = (Switch) findViewById(R.id.aesettingsShowtoolbarlabels);

		keepChanges.setChecked(AEHome.spAEdit.getBoolean("keepChanges", true));
		autodit.setChecked(AEHome.spAEdit.getBoolean("autodit", true));
		autoSave.setChecked(AEHome.spAEdit.getBoolean("autoSave", false));

		boolean showLabels = AEHome.spAEdit.getBoolean("showToolbarLabels", true);
		showToolBarLabels.setChecked(showLabels);
		setLabelsVisibility(showLabels);

		keepChanges.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
				@Override
				public void onCheckedChanged(CompoundButton cb, boolean c) {
					if (!c && AEHome.spAEdit.getString("tmpBuffer", "").isEmpty()==false) {
						AlertDialog.Builder dWarn = new AlertDialog.Builder(AESettings.this);
						dWarn.setTitle(R.string.discardQuestT);
						dWarn.setMessage(R.string.discardQuestM);
						dWarn.setPositiveButton(R.string.dBtConfirm, null);
						dWarn.setNegativeButton(R.string.dBtCancel, new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface d, int i) {
									keepChanges.setChecked(true);
								}
							});
						dWarn.show();
					}
				}
			});

		showToolBarLabels.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
			@Override
			public void onCheckedChanged(CompoundButton b, boolean c){
				setLabelsVisibility(c);
				AEHome.setLabelsVisibility(c);
			}
		});
    }

	@Override
	public void onBackPressed() {
		SharedPreferences.Editor ed = AEHome.spAEdit.edit();
		ed.putBoolean("keepChanges", keepChanges.isChecked());
		if (!keepChanges.isChecked()) {
			ed.putString("filePath", AEHome.newBuffer);
			ed.putString("tmpBuffer", "");
			for (String custom_name : AEdit.CUSTOM_NAMES) {
				ed.putString(custom_name, AEdit.CT_DEFAULT);
			}
			ed.putInt("selectionStart", 0);
			ed.putInt("selectionEnd", 0);
		}
		ed.putBoolean("autodit", autodit.isChecked());
		ed.putBoolean("autoSave", autoSave.isChecked());
		ed.putBoolean("showToolbarLabels", showToolBarLabels.isChecked());
		ed.apply();
		super.onBackPressed();
	}

	public void goBack(View v){
		onBackPressed();
	}

	public void openHMX(View v){
		Toast.makeText(this, "Olá :)", Toast.LENGTH_SHORT).show();
	}

	protected void setLabelsVisibility(boolean show){
		if (show) {
			barMenuRow2.setVisibility(View.VISIBLE);
		} else {
			barMenuRow2.setVisibility(View.GONE);
		}
	}
}
